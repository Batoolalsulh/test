<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',function(){
    return view('dashboard.index');
})->middleware('auth');
Route::resource('order','OrderController')->middleware('auth');
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::POST('/totalprice', 'ProductController@gettotal')->name('gettotalprice');
Route::get('export', 'OrderController@export')->name('export');

