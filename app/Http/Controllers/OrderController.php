<?php

namespace App\Http\Controllers;

use App\customer;
use App\Exports\OrderExport;
use App\order;
use App\order_product;
use App\product;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use OrderDetails;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     */
    public function index()
    {
      $orders=order::all();
      return view('dashboard.order',compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $customers=customer::all('id','name');
        $products=product::all('id','product_name');
        return view('dashboard.add-order',compact('customers','products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'prod'=>'required',
            'name'=>'required',
            'quan'=>'required'
        ]);
        $total=product::getthetotal($request['prod'],$request['quan']);
        $order=order::create([
         'customer_id'=>$request['name'],
         'total'=>$total,
        ]);
        $data = [];
        for($i = 0; $i < count($request['prod']); $i++) {
            $price=Product::find($request['prod'][$i])->price;
            $data[$i] = [
                'order_id' => $order->id,
                'product_id' => $request['prod'][$i],
                'quantity' => $request['quan'][$i],
                'price' =>$price,
            ];
        }
        $order->products()->attach($data);
        $request->session()->flash('success','Order Added successfully');
        return redirect('order');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(order $order)
    {
        $customers=customer::all('id','name');
        $products=product::all('id','product_name');
        $order_details= order_product::where('order_id',$order->id)->get();
        return view('dashboard.edit-order',compact('order_details','order','customers','products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, order $order)
    {
        $request->validate([
            'prod'=>'required',
            'name'=>'required',
            'quan'=>'required'
        ]);
        $total=product::getthetotal($request['prod'],$request['quan']);
        $order->update([
           'customer_id'=>$request['name'],
            'total'=>$total
        ]);
        $order_details=$order->products;
        foreach ($order_details as $key=>$or_details)
        {
            $or_details->pivot->update([
                'order_id'=>$order->id,
                'product_id'=>$request['prod'][$key],
                'quantity'=>$request['quan'][$key],
                'price'=>$or_details->price,
            ]);
        }
        $request->session()->flash('success','Order Updated Successfully!');
        return redirect('order');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(order $order)
    {
        $order_details=order_product::where('order_id',$order->id);
        $order_details->delete();
        $order->delete();
        return back()->with('success','Order Deleted Successfully');
    }

    public function export()
    {
        return Excel::download(new OrderExport, 'orders.xlsx');
    }
}
