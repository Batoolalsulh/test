<?php

namespace App\Http\Controllers;

use App\order_product;
use Illuminate\Http\Request;

class OrderProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\order_product  $order_details
     * @return \Illuminate\Http\Response
     */
    public function show(order_product $order_details)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\order_product  $order_details
     * @return \Illuminate\Http\Response
     */
    public function edit(order_product $order_details)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\order_product  $order_details
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, order_product $order_details)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\order_product  $order_details
     * @return \Illuminate\Http\Response
     */
    public function destroy(order_product $order_details)
    {
        //
    }
}
