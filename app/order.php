<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class order extends Model
{
   protected $fillable=['customer_id','total'];

    public function customer()
    {
        return $this->belongsTo(customer::class,'customer_id');
    }
    public function products()
    {
        return $this->belongsToMany(product::class,'order_products')->withPivot('quantity','price','created_at','updated_at');
    }
}
