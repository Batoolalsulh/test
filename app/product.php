<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class product extends Model
{
    public static function getthetotal($products,$quantity)
    {
        $total=0;
        foreach($products as $key=>$product)
        {
            $total+=$quantity[$key] * (DB::table('products')->select('price')->where('id',$product)->get()->first()->price);
        }
        return $total;
    }
    public function order()
     {
    $this->belongsToMany(order::class,'order_products');
     }
}
