<?php

namespace App\Exports;

use App\order;
use Maatwebsite\Excel\Concerns\FromCollection;

class OrderExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $orders=Order::all();
        $data=collect();
        foreach ($orders as $order)
        {
            $data->push([
                'order_id'=>$order->id,
                'customer_name'=>$order->customer->name,
                'date'=>$order->created_at
            ]);
        }
       return $data;
    }
}
