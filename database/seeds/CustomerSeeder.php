<?php

use Illuminate\Database\Seeder;
use App\customer;


class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\customer::class, 50)->create();
    }
}
