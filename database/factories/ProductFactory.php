<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\product;
use Faker\Generator as Faker;

$factory->define(product::class, function (Faker $faker) {
    return [
        'product_name'=>$faker->words(1,true),
        'price'=>$faker->randomFloat(2,1000,5000)
    ];
});
