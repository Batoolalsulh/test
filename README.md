## steps to run the project locally

- first of all you need to make database and name it whatever you want 
- rename .env.example file to .env and put your database information here
   
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=database_name
DB_USERNAME=root
DB_PASSWORD=
```



 **the first step** 

```
composer install
php artisan migrate
php artisan db:seed //this command to generate user and other needed data 

```
 **the second step**

 ```
 php artisan serve
 
 ```
 **the third step** 

when we request this link "localhost:8000" from the browser the login page will displayed , enter the credentials:

- email:batoolalsulh@gmai.com
- password:123456789

 **--api--**

## in the general way we have to use passport package to generate the access token but i do not see the need to use it in this simple test ##
so you can get the api directly
