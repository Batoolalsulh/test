@extends('layouts.layout')
@section('css')
    <link rel="stylesheet" href="{{asset('node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('../../node_modules/sweetalert2/dist/sweetalert2.min.css')}}">
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="card">
            <div class="card-body">
                <div class="col-sm-12 col-12 text-right my-2">
                    <a href="{{route('order.create')}}" class="btn btn btn-outline-success btn-rounded float-right"><i class="fa fa-plus"></i>Add Order</a>
                    <a class="mr-1 btn btn btn-outline-warning btn-rounded float-right " href="{{ route('export') }}">Export Order's Data</a>
                </div>
                <h4 class="card-title">Orders</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table id="order-listing" class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Customer Name</th>
                                    <th>date</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orders as $order)
                                    <tr>
                                        <td>{{$loop->index+1}}</td>
                                        <td>{{$order->customer->name}}</td>
                                        <td>{{$order->created_at}}</td>
                                        <td>
                                            <a class="btn btn-primary" href="{{route('order.edit',[$order->id])}}" ><i class="fa fa-pencil-square-o"></i></a>
                                            <button class="btn btn-danger" onclick="deleteItem({{$order->id}})"><i class="fa fa-trash"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                                @if (session('message'))
                                    <div class="alert alert-success">
                                        {{ session('message') }}
                                    </div>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <form method="post" action="" id="deleteForm">
        @csrf
        @method('DELETE')
    </form>
@endsection
@section('scripts')
    <script src="{{asset('node_modules/datatables.net/js/jquery.dataTables.js')}}"></script>
    <script src="{{asset('node_modules/datatables.net-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script src="node_modules/sweetalert2/dist/sweetalert2.min.js"></script>
    <script src="node_modules/jquery.avgrund/jquery.avgrund.min.js"></script>
    <script>
        var message="{{Session::get('success')}}";
        if(message!='')
        {
            swal(
                'Done!',
                message,
                'success'
            ).catch(swal.noop);
        }
    </script>
    <script>
        function deleteItem(id)
        {
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Delete '
            }).then((result) =>  {

                if (result) {

                    $('#deleteForm').attr('action','/order/'+id);
                    $('#deleteForm').submit();
                }
            }).catch(swal.noop);
        }
    </script>
    <script>
        $(document).ready( function () {
            $('.table').DataTable();
        } );
    </script>

@endsection
