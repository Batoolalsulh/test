@extends('layouts.layout')
@section('css')
    <link href="{{asset('css/select/select2.min.css')}}" rel="stylesheet" />
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Add order</h4>
                        <form method="post" action="{{route('order.store')}}">
                            @csrf
                            <div class="form-group">
                                <label for="name" class="col-sm-3 col-form-label">Customer Name</label>
                                <div class="col-sm-5">
                                    <select id="name" class="js-example-basic-single form-control" name="name" data-placeholder="Select Customer Name" >
                                        <option></option>
                                        @foreach($customers as $customer)
                                        <option value="{{$customer->id}}">{{$customer->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @error('name')
                                <div class="text-danger">
                                    <small><strong>{{ $message }}</strong></small>
                                </div>
                                @enderror
                            </div>
                            <div class="form-group" id="parent">
                                <label for="products" class="col-sm-3 col-form-label">Products</label>
                                <div class="form-group " style="display: flex">
                                <div class="col-sm-2">
                                    <select id="products" class="js-example-basic-single form-control" data-placeholder="Select Product" name="prod[]">
                                       <option></option>
                                        @foreach($products as $product)
                                            <option value="{{$product->id}}">{{$product->product_name}}</option>
                                        @endforeach
                                    </select>
                                    @error('prod[]')
                                    <div class="text-danger">
                                        <small><strong>{{ $message }}</strong></small>
                                    </div>
                                    @enderror
                                </div>
                                <div class="col-sm-2">
                                    <input class="form-control" type="number" name="quan[]" placeholder="Quantity" required>
                                </div>
                                <div class="col-sm-2">
                                    <button type="button" id="plus" onclick="addneworder()" class="btn btn-success">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                                </div>
                            </div>
                            <button type="button" class="btn btn-success" onclick="next()">Continue</button>
                            <div class="form-group hide" >
                                <label for="total" class="col-sm-3 col-form-label">Total</label>
                                <div class="col-sm-5">
                                    <input id="total" class="form-control" type="text" name="totalprice" readonly required>
                                </div>
                            </div>
                            <button type="submit"  class="btn btn-success hide">Add Order</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{asset('js/select/select2.min.js')}}"></script>
<script>
    $(document).ready(function(){
        $('.js-example-basic-single').select2();
        $('.hide').hide();
    });
    function addneworder()
    {
        $('#parent').append('<div class="form-group " style="display: flex">\n' +
            '                                <div class="col-sm-2">\n' +
            '                                    <select class="js-example-basic-single form-control" data-placeholder="Select Product"  name="prod[]"><option></option>\n' +
            '                                        @foreach($products as $product)\n' +
            '                                            <option value="{{$product->id}}">{{$product->product_name}}</option>\n' +
            '                                        @endforeach\n' +
            '                                    </select>\n' +
            '                                    @error('prod[]')\n' +
            '                                    <div class="<div class="text-danger">\n' +
            '                                        <strong>{{ $message }}</strong>\n' +
            '                                    </div>\n' +
            '                                    @enderror\n' +
            '                                </div>\n' +
            '                                <div class="col-sm-2">\n' +
            '                                    <input id="pool_name" class="form-control" type="number" name="quan[]" placeholder="Quantity" required>\n' +
            '                                </div>\n' +
            '                                <div class="col-sm-2">\n' +
            '                                    <button type="button" id="plus" onclick="addneworder()" class="btn btn-success">\n' +
            '                                        <i class="fa fa-plus"></i>\n' +
            '                                    </button>\n' +
            '                                </div>\n' +
            '                                </div>');
        $('.js-example-basic-single').select2();
        $('#parent').before(function(){
            $('#plus').replaceWith("<button type=\"button\" class=\"btn btn-danger\" onclick=\"$(this).parent().parent().remove()\">\n" +
                "                                        <i class=\"fa fa-minus\" id=\"minus\"></i>\n" +
                "                                    </button>");
        });
    }
    function next(){
        var array1=$('select[name="prod[]"]').map(function(){return $(this).val();}).get();
        var array2=$('input[name="quan[]"]').map(function(){return $(this).val();}).get();
        $('.hide').show();
        $.ajax({
            url :'{{route('gettotalprice')}}',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method:'post',
            data:{products:array1,quantities:array2},
            success : function (data) {
            $('#total').val(data+'$');
            }
        });
    }
</script>
@endsection
