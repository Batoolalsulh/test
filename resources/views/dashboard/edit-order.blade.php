@extends('layouts.layout')
@section('css')
    <link href="{{asset('css/select/select2.min.css')}}" rel="stylesheet" />
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Edit order</h4>
                        <form method="post" action="{{route('order.update',[$order->id])}}">
                            @csrf
                            @method('PATCH')
                            <div class="form-group">
                                <label for="name" class="col-sm-3 col-form-label">Customer Name</label>
                                <div class="col-sm-5">
                                    <select id="name" class="js-example-basic-single form-control" name="name" data-placeholder="Select Customer Name">
                                        @foreach($customers as $customer)
                                            @if ($order->customer_id==$customer->id)
                                                <option value="{{$customer->id}}" selected>{{$customer->name}}</option>
                                            @else
                                                <option value="{{$customer->id}}">{{$customer->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    @error('name')
                                    <div class="text-danger">
                                        <small><strong>{{ $message }}</strong></small>
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group" id="parent">
                                <label for="products" class="col-sm-3 col-form-label">Products</label>
                                @foreach($order_details as $details)
                                <div class="form-group " style="display: flex">
                                    <div class="col-sm-2">
                                        <select id="products" class="js-example-basic-single form-control" data-placeholder="Select Product" name="prod[]">
                                            @foreach($products as $product)
                                                @if($details->product_id==$product->id)
                                                <option value="{{$product->id}}" selected>{{$product->product_name}}</option>
                                                @else
                                                <option value="{{$product->id}}">{{$product->product_name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        @error('prod[]')
                                        <div class="text-danger">
                                            <small><strong>{{ $message }}</strong></small>
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="col-sm-2">
                                        <input class="form-control" type="number" name="quan[]" value="{{$details->quantity}}" placeholder="Quantity" required>
                                    </div>
                                    @if($loop->last)
                                    <div class="col-sm-2">
                                        <button type="button" id="plus" onclick="addneworder()" class="btn btn-success">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                    @else
                                        <div class="col-sm-2">
                                        <button type="button" class="btn btn-danger" onclick="$(this).parent().parent().remove()">
                                            <i class="fa fa-minus" id="minus"></i>
                                        </button>
                                        </div>
                                    @endif
                                </div>
                                @endforeach
                            </div>
                            <button type="button" class="btn btn-success" onclick="next()">Continue</button>
                            <div class="form-group hide" >
                                <label for="total" class="col-sm-3 col-form-label">Total</label>
                                <div class="col-sm-5">
                                    <input id="total" class="form-control" type="text" value="{{old('totalprice')}}" name="totalprice" readonly required>
                                </div>
                            </div>
                            <button type="submit"  class="btn btn-success hide">Edit Order</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{asset('js/select/select2.min.js')}}"></script>
    <script>
        $(document).ready(function(){
            $('.js-example-basic-single').select2();
            $('.hide').hide();
        });
        function addneworder()
        {
            $('#parent').append('<div class="form-group " style="display: flex">\n' +
                '                                <div class="col-sm-2">\n' +
                '                                    <select class="js-example-basic-single form-control"  data-placeholder="Select Product"  name="prod[]"><option></option>\n' +
                '                                        @foreach($products as $product)\n' +
                '                                            <option value="{{$product->id}}">{{$product->product_name}}</option>\n' +
                '                                        @endforeach\n' +
                '                                    </select>\n' +
                '                                    @error('prod[]')\n' +
                '                                    <div class="text-danger">\n' +
                '                                        <strong>{{ $message }}</strong>\n' +
                '                                    </span>\n' +
                '                                    @enderror\n' +
                '                                </div>\n' +
                '                                <div class="col-sm-2">\n' +
                '                                    <input id="pool_name" class="form-control" type="number" name="quan[]" placeholder="Quantity" required>\n' +
                '                                </div>\n' +
                '                                <div class="col-sm-2">\n' +
                '                                    <button type="button" id="plus" onclick="addneworder()" class="btn btn-success">\n' +
                '                                        <i class="fa fa-plus"></i>\n' +
                '                                    </button>\n' +
                '                                </div>\n' +
                '                                </div>');
            $('.js-example-basic-single').select2();
            $('#parent').before(function(){
                $('#plus').replaceWith("<button type=\"button\" class=\"btn btn-danger\" onclick=\"$(this).parent().parent().remove()\">\n" +
                    "                                        <i class=\"fa fa-minus\" id=\"minus\"></i>\n" +
                    "                                    </button>");
            });
        }
        function next(){
            var array1=$('select[name="prod[]"]').map(function(){return $(this).val();}).get();
            var array2=$('input[name="quan[]"]').map(function(){return $(this).val();}).get();
            $('.hide').show();
            $.ajax({
                url :'{{route('gettotalprice')}}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method:'post',
                data:{products:array1,quantities:array2},
                success : function (data) {
                    $('#total').val(data+'$');
                }
            });
        }
    </script>
@endsection
