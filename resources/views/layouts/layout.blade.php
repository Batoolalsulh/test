<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>ink codes</title>
    <!-- plugins:css -->
    <link rel="shortcut icon" href="{{ asset('images/unnamed.png') }}">
    <link rel="stylesheet" href="{{asset('node_modules/simple-line-icons/css/simple-line-icons.css')}}">
    <link rel="stylesheet" href="{{asset('node_modules/flag-icon-css/css/flag-icon.min.css')}}">
    <link rel="stylesheet" href="{{asset('node_modules/perfect-scrollbar/dist/css/perfect-scrollbar.min.css')}}">
    <link rel="stylesheet" href="{{asset('node_modules/ti-icons/css/themify-icons.css')}}">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <link rel="stylesheet" href="{{asset('node_modules/jquery-toast-plugin/dist/jquery.toast.min.css')}}">
    <link rel="stylesheet" href="{{asset('node_modules/font-awesome/css/font-awesome.min.css')}}" />
    <link rel="stylesheet" href="{{asset('node_modules/jquery-bar-rating/dist/themes/fontawesome-stars.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('../../node_modules/sweetalert2/dist/sweetalert2.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/styleE.css')}}">
    <link rel="stylesheet" href="{{asset('node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css')}}"/>
@yield('css')
</head>
<body>
<div class="container-scroller" id="app">
@include('layouts.navbar')
<!-- partial -->
    <div class="container-fluid page-body-wrapper">
        <div class="row row-offcanvas row-offcanvas-right">
        @include('layouts.sidebar')

        @yield('content')

        </div>

    </div>
</div>

<script src="{{asset('js/jquery.js')}}"></script>
<script src="{{asset('node_modules/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.min.js')}}"></script>
<script src="{{asset('node_modules/jquery-toast-plugin/dist/jquery.toast.min.js')}}"></script>
<script src="{{asset('node_modules/sweetalert2/dist/sweetalert2.min.js')}}"></script>
@yield('scripts')
</body>

</html>







