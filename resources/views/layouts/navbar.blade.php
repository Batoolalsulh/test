<nav id="navbar" class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
    <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
        <a class="navbar-brand brand-logo" href="/"><img src="{{asset('images/unnamed.png')}}" style="height: auto;" alt="logo"/></a>

    </div>
      <div class="navbar-menu-wrapper d-flex align-items-center">

        <ul class="navbar-nav navbar-nav-right">



          <li class="nav-item nav-settings d-none d-lg-block">
            <a class="nav-link" href="#" onclick="$('#logoutForm').submit();">
              <i class="fa fa-power-off"></i>
            </a>
          </li>
        </ul>

        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="icon-menu"></span>
        </button>
      </div>
    <form action="/logout" method="post" id="logoutForm">
             @csrf
         </form>
    </nav>
